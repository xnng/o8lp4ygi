const { pushCode } = require('./bitbucket')

try {
  const pushCodeLoop = async () => {
    await pushCode()
    setInterval(async () => {
      await pushCode()
    }, 40 * 60 * 1000)
  }

  pushCodeLoop()
} catch (error) {
  console.log(error)
}
